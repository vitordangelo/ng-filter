import { Country } from './../country';
import { GeonamesService } from './../geonames.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  countries: Country[] = [];
  countriesCopy: Country[] = [];
  service: GeonamesService;
  sortByNameFlag: boolean = true;
  sortByPopFlag: boolean = true;

  constructor(service: GeonamesService) {

    this.service = service;
    this.service
      .getAll()
      .subscribe(countries => {
        this.countries = countries;
        this.countriesCopy = countries;
      }, erro => console.log(erro));

  }

  ngOnInit() {
  }

  filterBy(filter) {
    switch (filter) {
      case 'all':
        return this.countries = this.countriesCopy;

      case 'europe':
        this.countries = this.countries.filter(country => {
          return country.continentName.toLocaleLowerCase().includes('europe');
        });
        break;

      case 'pop':
        this.countries = this.countriesCopy.filter(country => {
          return parseInt(country.population) > 201103320;
        });
        break;
    }
  }

  sortType(sort: string) {
    this.sortByNameFlag = !this.sortByNameFlag;
    this.sortByPopFlag = !this.sortByPopFlag;

    if (sort === 'name') {
      if(this.sortByNameFlag)
        this.countries = this.countries.sort(this.sortByCountryNameAsc);
      else this.countries = this.countries.sort(this.sortByCountryNameDesc);
    }

    if (sort === 'pop') {
      if(this.sortByPopFlag)
        this.countries = this.countries.sort(this.sortByPopAsc);
      else this.countries = this.countries.sort(this.sortByPopDesc);
    }    
  }

  sortByCountryNameAsc(c1: Country, c2: Country) {    
    if (c1.countryName > c2.countryName) return 1
      else if (c1.countryName === c2.continentName) return 0
      else return -1
  }

  sortByCountryNameDesc(c1: Country, c2: Country) {    
    if (c1.countryName < c2.countryName) return 1
      else if (c1.countryName === c2.continentName) return 0
      else return -1
  }

  sortByPopAsc(c1: Country, c2: Country) {
    return parseInt(c1.population) - parseInt(c2.population);
  }

  sortByPopDesc(c1: Country, c2: Country) {
    return parseInt(c1.population) + parseInt(c2.population);
  }
  

}
