import { Component, OnInit } from '@angular/core';
import { Country } from './../country';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  contry: Country = new Country();

  constructor() { }

  ngOnInit() {
  }

}
