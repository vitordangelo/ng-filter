import { Injectable } from '@angular/core';
import { Http, Headers, Response } from '@angular/http';
import { Observable } from 'rxjs';
import { Country } from './country';

@Injectable()
export class GeonamesService {

  http: Http;
  headers: Headers;
  url: string = 'http://api.geonames.org/countryInfoJSON?username=ermacmk';

  constructor(http: Http) { 
    this.http = http;
    this.headers = new Headers();
    this.headers.append('Content-Type', 'application/json');
  }

  getAll(): Observable<Country[]> {
    return this.http
      .get(this.url)
      .map(res => res.json().geonames);
  }

}