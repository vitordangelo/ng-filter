import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { MaterializeModule } from 'angular2-materialize';

import { AppRouting } from './app.routing';
import { HeaderComponent } from './header/header.component';
import { GeonamesService } from './geonames.service';
import 'rxjs/add/operator/map';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    HeaderComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    MaterializeModule,
    AppRouting
  ],
  providers: [
    GeonamesService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
