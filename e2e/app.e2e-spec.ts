import { NgFilterPage } from './app.po';

describe('ng-filter App', () => {
  let page: NgFilterPage;

  beforeEach(() => {
    page = new NgFilterPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
